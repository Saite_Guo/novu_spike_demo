'use client';
import { useState } from 'react';

export default function TriggerForm() {
    const [subscriberId, setSubscriberId] = useState('');
    const [message, setMessage] = useState('');

    const sendNotification = async (e: React.FormEvent) => {
        e.preventDefault();

        const response = await fetch('/api/send-notification', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                subscriberId,
                templateId: 'your_template_id',
                payload: { message },
            }),
        });

        const data = await response.json();
        console.log(data);
    };

    return (
        <div>
            <h1>Send Notification</h1>
            <form onSubmit={sendNotification}>
                <input
                    type="text"
                    placeholder="Subscriber ID"
                    value={subscriberId}
                    onChange={(e) => setSubscriberId(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Message"
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                />
                <button type="submit">Send</button>
            </form>
        </div>
    );
}
