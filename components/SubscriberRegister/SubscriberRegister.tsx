// pages/index.tsx
import { useState } from 'react';

export default function Home() {
    const [subscriberId, setSubscriberId] = useState('');
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [phone, setPhone] = useState('');
    const [avatar, setAvatar] = useState('');
    const [locale, setLocale] = useState('');
    const [data, setData] = useState('');

    const addSubscriber = async (e: React.FormEvent) => {
        e.preventDefault();

        const response = await fetch('/api/add-subscriber', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                subscriberId,
                email,
                firstName,
                lastName,
                phone,
                avatar,
                locale,
                data: JSON.parse(data || '{}'),
            }),
        });

        const result = await response.json();

    };

    return (
        <div>
            <h1>Add Subscriber</h1>
            <form onSubmit={addSubscriber}>
                <input
                    type="text"
                    placeholder="Subscriber ID"
                    value={subscriberId}
                    onChange={(e) => setSubscriberId(e.target.value)}
                    required
                />
                <input
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="First Name"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Last Name"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Phone"
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Avatar URL"
                    value={avatar}
                    onChange={(e) => setAvatar(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Locale"
                    value={locale}
                    onChange={(e) => setLocale(e.target.value)}
                />
                <textarea
                    placeholder="Custom Data (JSON format)"
                    value={data}
                    onChange={(e) => setData(e.target.value)}
                ></textarea>
                <button type="submit">Add Subscriber</button>
            </form>
        </div>
    );
}
