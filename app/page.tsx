'use client';
import TriggerForm from '@/components/TriggerForm';
import SubscriberRegister from '@/components/SubscriberRegister';

export default function Home() {
  return (
    <main>
      <div className='text-2xl flex justify-center flex-col align-middle'>
        <h1>Novu</h1>
        <p>Send notifications to your subscribers.</p>
      </div>
      <div>
        <TriggerForm />
      </div>

      <div>
        <SubscriberRegister />
      </div>

    </main>
  );
}