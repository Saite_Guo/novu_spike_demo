// pages/api/send-notification.ts
import type { NextApiRequest, NextApiResponse } from 'next';
import novu from '.';

type Data = {
  message?: string;
  error?: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === 'POST') {
    const { subscriberId, templateId, payload } = req.body;

    try {
      const response = await novu.trigger(templateId, {
        to: {
          subscriberId,
        },
        payload,
      });
      res.status(200).json({ message: 'Notification sent successfully!' });
    } catch (error) {
      res.status(500).json({ error: (error as Error).message });
    }
  } else {
    res.status(405).end(); 
  }
}
