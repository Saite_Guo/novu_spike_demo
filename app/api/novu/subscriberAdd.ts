// pages/api/add-subscriber.ts
import type { NextApiRequest, NextApiResponse } from 'next';
import novu from '.';

type Data = {
  message?: string;
  error?: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === 'POST') {
    const { subscriberId, email, firstName, lastName, phone, avatar, locale, data } = req.body;

    try {
      await novu.subscribers.identify(subscriberId, {
        email,
        firstName,
        lastName,
        phone,
        avatar,
        locale,
        data,
      });
      res.status(200).json({ message: 'Subscriber added successfully!' });
    } catch (error) {
      res.status(500).json({ error: (error as Error).message });
    }
  } else {
    res.status(405).end(); // Method Not Allowed
  }
}
